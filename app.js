require('dotenv').config();
const db = require('./db-utils/app-db');
const env_db = require('./db-utils/env-db');
const express = require('express')
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const key = process.env.SECRET_KEY;
const auth = require("./middleware/auth");

var app = express()

app.use(express.json())

env_db.setupEnv();

app.post('/register', function (req, res) {
  const name = req.body.name;
  const email = req.body.email;
  var password = req.body.password;

  if (!(email && password && name)) return res.status(400).send({error: 'All input is required'});
  password = bcrypt.hashSync(req.body.password);
  db.createUser([name, email, password], (err) => {
    if (err) return res.status(500).send({error: err, message: err.message});
    db.findUserByEmail(email, (err, user) => {
        if (err) return res.status(500).send({error: err});
        const expiresIn = 24 * 60 * 60;
        const accessToken = jwt.sign({ id: user.id }, key, {
            expiresIn: expiresIn
        });
        res.status(200).send({user: user, access_token: accessToken});
    });
  });
})

app.post('/login', (req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  if (!(email && password)) return res.status(400).send({error:'All input is required'});
  db.findUserByEmail(email, (err, user) => {
      if (err) return res.status(500).send({error: err});
      if (!user) return res.status(404).send({error: 'User not found!'});
      const result = bcrypt.compareSync(password, user.password);
      if (!result) return res.status(401).send({error: 'Password not valid!'});

      const expiresIn = 24 * 60 * 60;
      const accessToken = jwt.sign({ id: user.id }, key, {
          expiresIn: expiresIn
      });
      res.status(200).send({user: user, access_token: accessToken });
  });
});

app.post('/admin', auth, (req, res) => {
  db.isAdmin(req.user.id, (err, user) => {
    if (err) return res.status(500).send({auth: false, error: err});
    if (!user.length) return res.status(401).send({auth: false, error: 'User is not admin'});
    res.status(200).send({auth: true});
  });
});

app.post('/admin/add_role', auth, (req, res) => {
  db.isAdmin(req.user.id, (err, user) => {
      if (err) return res.status(500).send({error: err});
      if (!user.length) return res.status(401).send({auth: false, error: 'User is not admin'});

      db.addRole(req.body.data, (err) => {
          if (err) return res.status(500).send({error: err});
          res.status(200).send({msg: 'Role added'});
      });
  });
});

app.post('/admin/remove_role', auth, (req, res) => {
  db.isAdmin(req.user.id, (err, user) => {
    if (err) return res.status(500).send({error: err});
    if (!user.length) return res.status(401).send({auth: false, error: 'User is not admin'});

    db.removeRole(req.body.data, (err) => {
        if (err) return res.status(500).send({error: err});
        res.status(200).send({msg: 'Role removed'});
    });
  });
});

app.post('/user', auth, (req, res) => {
  db.isUser(req.user.id, (err, user) => {
    if (err) return res.status(500).send({error: err});
    if (!user.length) return res.status(401).send({error: 'User is not allowed'});
    res.status(200).send({auth: true});
  });
});

app.post('/user/change_password', auth, (req, res) => {
  const new_password = req.body.new_password;
  if (!(new_password)) return res.status(400).send({error: 'All input is required'});
  db.changePassword([new_password, req.user.id], (err) => {
    if (err) return res.status(500).send({error: err});
    res.status(200).send({msg: 'Password changed'});
  });
});

app.post('/list', (req, res) => {
  const list = db.listUsers((err, data) => {
    if (err) return res.status(500).send({error: err});
    res.status(200).send(data);
  });
});

app.get('/', (req, res) => {
    res.status(200).send('This is an authentication server');
});

module.exports = app;
