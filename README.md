## basic commands
 - install packages: npm install
 - start server: npm run start
 - start server in dev mode: npm run dev
 - run test: npm run test

## defined routes

route: /register
method: post
params: name, email, password
action: create a new user
returns: {user, access_token}

route: /login
method: post
params: email, password
action: create a new user
returns: {user, access_token}

route: /admin
method: post
params: token
action: validate token and accept only valid token of admin role
returns: {auth: true}

route: /admin/add_role
method: post
params: token, id_user, id_role
action: validate token and add role for user
returns: {msg: 'Role added'}

route: /admin/remove_role
method: post
params: token, id_user, id_role
action: validate token and remove role for user
returnd: {msg: 'Role removed'}

route: /user
method: post
params: token
action: validate token and accept only valid token of user role
returns: {auth: true}

route: /user/change_password
method: post
params: token, new_password
action: validate token and changes password of user encoded in token
returns: {msg: 'Password changed'}
