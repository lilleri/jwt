const jwt = require('jsonwebtoken');
require('dotenv').config();
const db = require('../db-utils/app-db');
const key = process.env.SECRET_KEY;

const verifyToken = (req, res, next) => {
  const token = req.body.token || req.query.token || req.headers['x-access-token'];
  if (!token) {
    return res.status(403).send('A token is required for authentication');
  } else {
    try {
      const decoded = jwt.verify(token, key);
      req.user = decoded;
    } catch (err) {
      return res.status(401).send({auth: false, error: 'Invalid Token'});
    }

  }
  return next();
};

module.exports = verifyToken;
