const sqlite3 = require('sqlite3').verbose();
const database = new sqlite3.Database("./my.db");
const bcrypt = require('bcryptjs');

const createUsersTable = (cb) => {
  const sqlQuery = 'CREATE TABLE IF NOT EXISTS users (id integer PRIMARY KEY,name text,email text UNIQUE,password text)';
  return database.run(sqlQuery, cb);
}
const cleanUsersTable = (cb) => {
  const sqlQuery = 'DELETE FROM users WHERE 1';
  return database.run(sqlQuery, cb);
}

const populateUsersTable = (cb) => {
  const sqlQuery = 'INSERT INTO users (id, name, email, password) VALUES (1, \'adminName\', \'adminEmail\', \''+bcrypt.hashSync('adminPwd')+'\'), (2, \'userName\', \'userEmail\', \''+bcrypt.hashSync('userPwd')+'\'), (3, \'editorName\', \'editorEmail\', \''+bcrypt.hashSync('editorPwd')+'\'), (4, \'lastName\', \'lastEmail\', \''+bcrypt.hashSync('lastPwd')+'\')';
  return database.run(sqlQuery, cb);
}

const createRolesTable = (cb) => {
  const sqlQuery = 'CREATE TABLE IF NOT EXISTS roles (id integer PRIMARY KEY,name text)';
  return database.run(sqlQuery, cb);
}
const cleanRolesTable = (cb) => {
  const sqlQuery = 'DELETE FROM roles WHERE 1';
  return database.run(sqlQuery, cb);
}

const populateRolesTable = (cb) => {
  const sqlQuery = 'INSERT INTO roles (id, name) VALUES (1, \'admin\'), (2, \'user\'), (3, \'editor\')';
  return database.run(sqlQuery, cb);
}

const createUsersRolesTable = (cb) => {
  const sqlQuery = 'CREATE TABLE IF NOT EXISTS users_roles (id integer PRIMARY KEY,id_user integer, id_role integer)';
  return database.run(sqlQuery, cb);
}

const cleanUsersRolesTable = (cb) => {
  const sqlQuery = 'DELETE FROM users_roles WHERE 1';
  return database.run(sqlQuery, cb);
}

const populateUsersRolesTable = (cb) => {
  const sqlQuery = 'INSERT INTO users_roles (id_user, id_role) VALUES (1, 1), (1, 2), (1,3), (2, 2), (3,3), (4,2), (4,1)';
  return database.run(sqlQuery);
}

const setupEnv = () => {
  database.serialize(() => {
    createUsersTable((err) => {
      if (err) return res.status(500).send({'error': err, 'message': err.message});
      cleanUsersTable((err) => {
        if (err) return res.status(500).send({'error': err, 'message': err.message});
        populateUsersTable((err) => {
          if (err) return res.status(500).send({'error': err, 'message': err.message});
        });
      });
    });

    createRolesTable((err) => {
      if (err) return res.status(500).send({'error': err, 'message': err.message});
      cleanRolesTable((err) => {
        if (err) return res.status(500).send({'error': err, 'message': err.message});
        populateRolesTable((err) => {
          if (err) return res.status(500).send({'error': err, 'message': err.message});
        });
      });
    });

    createUsersRolesTable((err) => {
      if (err) return res.status(500).send({'error': err, 'message': err.message});
      cleanUsersRolesTable((err) => {
        if (err) return res.status(500).send({'error': err, 'message': err.message});
        populateUsersRolesTable((err) => {
          if (err) return res.status(500).send({'error': err, 'message': err.message});
        });
      });
    });
  });

}

exports.setupEnv = setupEnv;
