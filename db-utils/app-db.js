const sqlite3 = require('sqlite3').verbose();
const database = new sqlite3.Database("./my.db");

const findUserByEmail = (email, cb) => {
  const sqlQuery = 'SELECT * FROM users WHERE email = ?';
    return database.get(sqlQuery, [email], (err, row) => {
        cb(err, row)
    });
}

const createUser = (user, cb) => {
  const sqlQuery = 'INSERT INTO users (name, email, password) VALUES (?,?,?)';
    return database.run(sqlQuery, user, (err) => {
        cb(err)
    });
}

const listUsers = (cb) => {

  let sqlQuery = 'SELECT * FROM users';
  return database.all(sqlQuery, [], (err, data) => {
      cb(err, data)
  });
}

const isAdmin = (id, cb) => {
  let sqlQuery = 'SELECT * FROM users_roles WHERE id_user = ? AND id_role = 1';
  return database.all(sqlQuery, [id], (err, data) => {
      cb(err, data)
  });
}

const isUser = (id, cb) => {
  let sqlQuery = 'SELECT * FROM users_roles WHERE id_user = ? AND id_role = 2';
  return database.all(sqlQuery, [id], (err, data) => {
      cb(err, data)
  });
}

const changePassword = (data, cb) => {
  const sqlQuery = 'UPDATE users SET password = ? WHERE id = ?';
    return database.run(sqlQuery, data, (err) => {
        cb(err)
    });
}

const addRole = (data, cb) => {
  const sqlQuery = 'INSERT INTO users_roles (id_user, id_role) VALUES (?,?)';
    return database.run(sqlQuery, data, (err) => {
        cb(err)
    });
}

const removeRole = (data, cb) => {
  //console.log(data);
  const sqlQuery = 'DELETE FROM users_roles WHERE id_user = ? AND id_role= ?';
    return database.run(sqlQuery, data, (err) => {
        cb(err)
    });
}

exports.findUserByEmail = findUserByEmail;
exports.createUser = createUser;
exports.listUsers = listUsers;
exports.isAdmin = isAdmin;
exports.isUser = isUser;
exports.changePassword = changePassword;
exports.addRole = addRole;
exports.removeRole = removeRole;
