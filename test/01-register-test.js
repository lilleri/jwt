var assert = require('assert');
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index');
let should = chai.should();
chai.use(chaiHttp);

describe('/register', function() {
  it('it should not register user without email field', (done) => {
    chai.request(server)
        .post('/register')
        .send({password:'pwd', name:'name'})
        .end((err, res) => {
              res.should.have.status(400);
          done();
        });
  });
  it('it should register new user', (done) => {
    let r = (Math.random() + 1).toString(36);
    chai.request(server)
        .post('/register')
        .send({password: 'pwd', name: 'name', email: r+'name@gmail.com'})
        .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.should.have.property('user');
              res.body.user.should.be.a('object');
              res.body.user.should.have.property('id');
              res.body.should.have.property('access_token');
              res.body.access_token.should.be.a('string');
          done();
        });

  });
});
