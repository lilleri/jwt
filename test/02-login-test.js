var assert = require('assert');
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index');
let should = chai.should();
chai.use(chaiHttp);

describe('/login', function() {
  it('it should not register user without email field', (done) => {
    chai.request(server)
        .post('/register')
        .send({email: 'email'})
        .end((err, res) => {
              res.should.have.status(400);
          done();
        });
  });
  it('it should register new user and create access_token', (done) => {
    let r = (Math.random() + 1).toString(36);
    email=r+'name@gmail.com';
    pwd='pwd';

      let user = {
          password: pwd,
          name: 'name',
          email: email
      }
    chai.request(server)
        .post('/register')
        .send(user)
        .end((err, res) => {
            chai.request(server)
                .post('/login')
                .send({email: email, password:pwd})
                .end((err, res) => {
                      res.should.have.status(200);
                      res.body.should.be.a('object');
                      res.body.should.have.property('user');
                      res.body.should.have.property('access_token');
                  done();
                });
        });

  });
});
