var assert = require('assert');
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index');
let should = chai.should();
chai.use(chaiHttp);
const db = require('../db-utils/app-db');

describe('/admin/add_role', function() {

  it('it should add a role', (done) => {
    chai.request(server)
      .post('/login')
      .send({password: 'adminPwd',email: 'adminEmail'})
      .end((err, res) => {
        chai.request(server)
          .post('/admin/add_role')
          .send({token: res.body.access_token, data:[3,1]})
          .end((err, res) => {
            res.should.have.status(200);
            db.isAdmin(3, (err, user) => {
              assert.equal(true, user.length);
            });
            done();
          });
      });
  });

  it('it should remove a role', (done) => {
    chai.request(server)
      .post('/login')
      .send({password: 'adminPwd',email: 'adminEmail'})
      .end((err, res) => {
        chai.request(server)
          .post('/admin/remove_role')
          .send({token: res.body.access_token, data:[4,1]})
          .end((err, res) => {
            res.should.have.status(200);
            db.isAdmin(4, (err, user) => {
              assert.equal(false, user.length);
            });
            done();
          });
      });
  });

});
