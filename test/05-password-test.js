var assert = require('assert');
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index');
let should = chai.should();
chai.use(chaiHttp);

describe('/user/change_password', function() {
  it('it should reject with 403 without token', (done) => {
    chai.request(server)
        .post('/user/change_password')
        .end((err, res) => {
              res.should.have.status(403);
              res.should.have.property('text');
              assert.equal(res.text,'A token is required for authentication');
              done();
        });
  });
  it('it should reject with 401 not valid token', (done) => {
    chai.request(server)
        .post('/user/change_password')
        .send({token:'abc'})
        .end((err, res) => {
              res.should.have.status(401);
              res.should.have.property('text');
              assert.equal(res.text,'{"auth":false,"error":"Invalid Token"}');
        done();

        });
  });
  it('it should reject with 400 if no new_password', (done) => {
    chai.request(server)
    .post('/login')
    .send({password: 'userPwd',email: 'userEmail'})
    .end((err, res) => {
        chai.request(server)
            .post('/user/change_password')
            .send({token: res.body.access_token})
            .end((err, res) => {
                  res.should.have.status(400);
                  res.should.have.property('text');
                  assert.equal(res.text,'{"error":"All input is required"}')
              done();
            });
    });
  });


  it('it update password', (done) => {
    chai.request(server)
        .post('/login')
        .send({password: 'userPwd', email: 'userEmail'})
        .end((err, res) => {
            chai.request(server)
                .post('/user/change_password')
                .send({token: res.body.access_token, new_password: 'newPassword'})
                .end((err, res) => {
                  res.should.have.status(200);
                  done();
                });
        });
  });

});
